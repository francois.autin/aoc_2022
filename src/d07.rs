use std::slice::Iter;

fn parse_dir(prefix: &String, dir_desc: &str) -> (String, i32, Vec<String>) {

    let mut lines = dir_desc.lines();
    let prefix = prefix.to_owned();
    let dir_path = {
        let mut p = prefix + lines.next().unwrap_or_default();
        if p.as_str() != "/" {
            p.push('/');
        }
        p
    };
    let mut size = 0;
    let mut subdirs = Vec::<String>::new();
    
    lines.for_each(|line| {
        let x = line.split_once(' ').unwrap_or_default();
        if x.0 == "dir" {
            let mut subdir_path = dir_path.clone();
            subdir_path.push_str(x.1);
            subdir_path.push('/');
            subdirs.push(subdir_path);
        } else {
            size += x.0.parse::<i32>().unwrap_or_default();
        }
    });

    (dir_path, size, subdirs)

}

fn get_tree(input: &str) -> Vec<(String, i32, Vec<String>)> {
    let mut current_path = String::new();
    let lss: Vec<&str> = input.split_inclusive("$ cd").map(|x| x.trim_start_matches("$ cd").trim()).filter(|x| !x.is_empty()).collect();
    let mut directories = Vec::<(String, i32, Vec<String>)>::new();

    for item in lss {
        if item.starts_with("..") {
            current_path.pop();
            while !current_path.ends_with('/') { let _ = current_path.pop(); };
        } else {
            directories.push(parse_dir(&current_path, item));
            current_path = directories.last().unwrap().0.clone();
        }
    };

    directories
}

fn size_of_dir(dir: &(String, i32, Vec<String>), iter: &Iter<(String, i32, Vec<String>)>) -> i32 {

    let mut iter2 = iter.clone();
    dir.1 + dir.2.iter().map(|x| {
        match iter2.find(|elem| elem.0 == *x) {
            Some(subdir) => size_of_dir(subdir, iter),
            None => 0
        }
        
    }).sum::<i32>()

}

pub fn part1(input: &str) {
    let directories = get_tree(input);
    let sum = directories.iter().map(|x| {
        let iter = directories.iter();
        size_of_dir(x, &iter)
    }).filter(|x| 100000 >= *x).sum::<i32>();
    println!("{}", sum)
}

pub fn part2(input: &str) {
    let directories = get_tree(input);
    let dir_sizes = directories.iter().map(|x| {
        let iter = directories.iter();
        size_of_dir(x, &iter)
    });
    let root_size = dir_sizes.clone().max().unwrap_or_default();
    let min = dir_sizes.filter(|x| root_size - *x <= 40000000).min().unwrap_or_default();
    println!("{}", min)
}
