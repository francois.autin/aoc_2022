use std::{collections::HashSet, str::Lines};

fn set_priority(c: &char) -> u32 {

    match c.is_ascii_lowercase() {
        true => (*c as u32) - 96,
        false => (*c as u32) - 38
    }

}

pub fn part1(input: &str)
{
    fn part1_line(line: &str) -> u32 {
        let (compartment1, compartment2) = line.split_at(line.len()/2);
        let chars: HashSet<char> = compartment1.chars().collect();
        chars.iter().filter(|x| compartment2.contains(**x)).map(set_priority).sum()

    }

    let result = input.lines().fold(0, |acc, x| acc + part1_line(x));
    
    println!("{}", result);
}

pub fn part2(input: &str)
{
    fn part2_compute(input_iterator: &mut Lines) -> u32 {

        let mut sum = 0;

        while let Some(first_rucksack) = input_iterator.next() {

            // Adding each type of item to a Set
            let mut rucksacks = Vec::<HashSet<char>>::new();
            rucksacks.push(first_rucksack.chars().collect());
            for _ in 0..2 { rucksacks.push(input_iterator.next().unwrap().chars().collect()) };
            // Adding the Priority of the item type found in all three rucksacks to the total
            sum += rucksacks[0].iter().filter(|x| rucksacks.iter().map(|y| y.contains(x)).all(|x| x)).map(set_priority).sum::<u32>()

        };

        sum

    }

    let mut input_iterator = input.lines();

    println!("{}", part2_compute(&mut input_iterator))
}