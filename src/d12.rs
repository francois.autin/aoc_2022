use std::{collections::{VecDeque, HashSet}};

type Parent = Option<Box<Point>>;

#[derive(Debug, Eq, Hash, PartialEq, Clone)]
struct Point(usize, usize, Parent);

impl Point
{
    fn up(&self) -> Option<Point>
    {
        if self.1 == 0 { None }
        else { Some(Point(self.0, self.1 - 1, None)) }
    }

    fn down<T>(&self, map: &Vec<Vec<T>>) -> Option<Point>
    {
        if self.1 == map.len().saturating_sub(1) { None }
        else { Some(Point(self.0, self.1 + 1, None)) }
    }

    fn left(&self) -> Option<Point>
    {
        if self.0 == 0 { None }
        else { Some(Point(self.0 - 1, self.1, None)) }
    }

    fn right<T>(&self, map: &[Vec<T>]) -> Option<Point>
    {
        let row = map.get(self.1)?;
        if self.0 == row.len().saturating_sub(1) { None }
        else { Some(Point(self.0 + 1, self.1, None)) }
    }

    fn is_accessible(&self, from: &Point, map: &[Vec<char>]) -> bool
    {
        let f = map[from.1][from.0];
        let mut t = map[self.1][self.0];
        if t == 'E' { t = 'z' }
        let x = t as i32 - f as i32;
        x < 2
    }

    fn set_parent(&mut self, parent: &Point)
    {
        self.2 = Some(Box::new(parent.clone()));
    }
}

fn get_map(input: &str) -> Vec<Vec<char>>
{
    input.lines().map(|l| { let v: Vec<char> = l.chars().collect(); v}).collect()
}

fn get_starting_coord(marker: char, map: &[Vec<char>]) -> Option<Point>
{
    for (y, item) in map.iter().enumerate() {
        for (x, c) in item.iter().enumerate() {
            if *c == marker {
                return Some(Point(x, y, None))
            }
        }
    };
    None
}

fn has_best_signal(coord: &Point, map: &[Vec<char>]) -> bool
{
    if let Some(row) = map.get(coord.1) {
        if let Some(c) = row.get(coord.0) {
            if *c == 'E' { return true }
        }
    }
    false
}

fn is_a(coord: &Point, map: &[Vec<char>]) -> bool
{
    if let Some(row) = map.get(coord.1) {
        if let Some(c) = row.get(coord.0) {
            if *c == 'a' { return true }
        }
    }
    false
}

fn explore_part1(map: &Vec<Vec<char>>, root: Point) -> Option<usize>
{
    let mut queue = VecDeque::<Point>::new();
    let mut explored = HashSet::<Point>::new();
    explored.insert(root.clone());
    queue.push_back(root);
    while !queue.is_empty() {
        let v = queue.pop_front().unwrap();
        if has_best_signal(&v, map) {
            let mut l = 0;
            let mut node = v;
            while let Some(n) = node.2 { l += 1;  node = *n };
            return Some(l) }
        else {
            for mut next in [v.up(), v.down(map), v.left(), v.right(map)]
                            .iter().filter_map(|x| x.clone())
                                   .filter(|x| x.is_accessible(&v, map))
            {
                if !explored.contains(&next) {
                    explored.insert(next.clone());
                    next.set_parent(&v);
                    queue.push_back(next)
                }
            }
        }
    };
    None
}

fn explore_part2(map: &Vec<Vec<char>>, root: Point) -> Option<usize>
{
    let mut queue = VecDeque::<Point>::new();
    let mut explored = HashSet::<Point>::new();
    explored.insert(root.clone());
    queue.push_back(root);
    while !queue.is_empty() {
        let v = queue.pop_front().unwrap();
        if is_a(&v, map) {
            let mut l = 0;
            let mut node = v;
            while let Some(n) = node.2 { l += 1;  node = *n };
            return Some(l) }
        else {
            for mut next in [v.up(), v.down(map), v.left(), v.right(map)]
                            .iter().filter_map(|x| x.clone())
                                   .filter(|x| v.is_accessible(x, map))
            {
                if !explored.contains(&next) {
                    explored.insert(next.clone());
                    next.set_parent(&v);
                    queue.push_back(next)
                }
            }
        }
    };
    None
}

pub fn part1(input: &str)
{
    let mut map = get_map(input);
    let starting_coord = match get_starting_coord('S', &map) {
                            Some(c) => c,
                            None => panic!("Malformed input")
                        };
    map[starting_coord.1][starting_coord.0] = 'a';
    let result = explore_part1(&map, starting_coord);
    println!("{result:?}");
}

pub fn part2(input: &str)
{
    let mut map = get_map(input);
    let starting_coord = match get_starting_coord('E', &map) {
                            Some(c) => c,
                            None => panic!("Malformed input")
                        };
    map[starting_coord.1][starting_coord.0] = 'z';
    let result = explore_part2(&map, starting_coord);
    println!("{result:?}");
}