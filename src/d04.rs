fn turn_string_slice_into_int_range(slice: &str) -> (u16, u16) {
    // Splitting the string into substrings separated by '-',
    // then mapping to these the parsed unsigned 16 bit integer,
    // then collecting them into a Vector
    let mut map: Vec<u16> = slice.split_terminator(|x| x == '-')
                                 .map(|x| x.parse::<u16>().unwrap())
                                 .collect();
    // Sorting the vector so we can get the smallest and highest value in order
    // We could also have used the Iterator::{min, max} functions
    // but that would have required creating an Iterator, while
    // here we only sort 2 values (very fast)
    map.sort();
    // Vectors store references, but we want values instead. So we dereference them using *
    (*(map.first().unwrap()), *(map.last().unwrap()))
}

fn contains(r1: (u16, u16), r2: (u16, u16)) -> bool {
    // Checking if r2's borders are contained within r1
    r1.0 <= r2.0 && r1.1 >= r2.1
}

fn overlaps(r1: (u16, u16), r2: (u16, u16)) -> bool {
    // Same as contains but we also want to check whether only a single border is contained within the other
    !(r1.0 > r2.0 || r1.1 < r2.1 && r2.0 > r1.1) ||
    (r1.0 <= r2.1 && r2.1 <= r1.1)
}

fn part_x(input: &str, f: &dyn Fn((u16, u16), (u16, u16)) -> bool) -> i32 {

    // This function takes a line from the input, finds the two ranges (can work for any ammount of ranges),
    // then says if one of these ranges is overlaps or is contained within all others
    fn one_assignment_f_the_other(line: &str, f: &dyn Fn((u16, u16), (u16, u16)) -> bool) -> bool {

        // Splitting the string at ','. split_terminator returns an iterator over all relevant subslices
        let split_string = line.split_terminator(|x| x == ',');
        // Mapping to these subslices the corresponding ranges and collecting them in a vector
        let ranges: Vec<(u16, u16)> = split_string.map(turn_string_slice_into_int_range).collect();
        
        // For all ranges of the ranges Vector, map true if said range is contained or overlaps all other ranges in the Vector
        // If any range is mapped to true, return true
        ranges.iter()
            .map(|current_range| {
                ranges.iter()
                    .map(|range_to_check_against| {
                        f(*current_range, *range_to_check_against)
                    })
                    .all(|x| x)
            })
            .any(|x| x)

    }

    // For each line of the input, map check whether there is one
    // range which is contained within or overlaps all others,
    // then sum up the number of lines where this occurs
    input.lines()
        .map(|x| {
            one_assignment_f_the_other(x, &f)
        })
        .map(|x| { 
            if x { 1 } else { 0 }
        })
        .sum()

}

pub fn part1(input: &str)
{
    println!("{}", part_x(input, &contains));
}

pub fn part2(input: &str)
{
    println!("{}", part_x(input, &overlaps));
}
