fn is_marker(window: &str) -> bool {
    !window.chars().any(
        |x| 1 != window.chars().map(|y| (y == x) as i32).sum()
    )
}

fn recursive_index_search(buffer: &str, block_size: usize) -> u32 {

    let current_window = buffer.split_at(block_size).0;
    match is_marker(current_window) {
        true => block_size as u32,
        false => 1 + recursive_index_search(buffer.split_at(1).1, block_size)
    }

}

pub fn part1(buffer: &str) { println!("{}", recursive_index_search(buffer, 4)) }

pub fn part2(buffer: &str) { println!("{}", recursive_index_search(buffer, 14)) }