fn get_count(s: &str) -> Vec<i32>
{
    let mut vec = Vec::<i32>::new();
    let mut count = 0;

    s.lines().for_each(
        |x| {
            if x.is_empty() {
                vec.push(count);
                count = 0
            } else {
                count += x.parse::<i32>().unwrap()
            }
    });
    vec.sort();
    vec
}

pub fn part1(s: &str)
{
    println!("{}", get_count(s).last().unwrap());
}

pub fn part2(s: &str)
{
    let vec = get_count(s);
    let three_elves: i32 = vec.split_at(vec.len()-3).1.iter().sum();
    println!("{}", three_elves);
}