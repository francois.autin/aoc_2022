use std::{iter::Peekable, cmp::Ordering};

#[derive(Debug, Eq, PartialEq)]
enum Tree {
    Leaf(i32),
    Node(Vec<Tree>)
}

impl PartialOrd for Tree {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        use Tree::*;
        use Ordering::*;
        match (self, other) {
            (Leaf(a), Leaf(b)) => Some(a.cmp(b)),
            (Leaf(a), Node(_)) => Node(vec!(Leaf(*a))).partial_cmp(other),
            (Node(_), Leaf(a)) => self.partial_cmp(&Node(vec!(Leaf(*a)))),
            (Node(l1), Node(l2)) => {
                let pairs = l1.iter().zip(l2.iter());
                let comps = pairs.map(|p| p.0.partial_cmp(p.1).unwrap()).collect::<Vec<Ordering>>();
                if comps.iter().all(|x| x == &Equal)  {
                    Some(l1.len().cmp(&l2.len()))
                } else {
                    comps.iter().filter(|x| x != &&Equal).map(|x| *x).next()
                }
            }
        }
    }
}

fn next_int<T>(packet: &mut T) -> i32
where T: Iterator<Item = char> {
    let mut buffer = String::new();
    while let Some(c) = packet.next() {
        if !c.is_numeric() {
            break;
        } else {
            buffer.push(c);
        }
    }
    str::parse::<i32>(&buffer).unwrap()
}

fn build_tree<T>(packet: &mut Peekable<T>) -> Option<Tree>
where T: Iterator<Item = char> {
    let next_char = packet.peek();
    match next_char {
        None => None,
        Some(c) => {
            if c == &'[' {
                let mut node_list = Vec::<Tree>::new();
                packet.next();
                while let Some(t) = build_tree(packet) {
                    node_list.push(t);
                }
                Some(Tree::Node(node_list))
            } else if c == &']' || c == &',' {
                packet.next();
                None
            } else {
                Some(Tree::Leaf(next_int(packet)))
            }
        }
    }
}

fn build_tree_from_str(input: &str) -> Option<Tree> {
    let valid_char = |c: &char| c.is_numeric() || c == &'[' || c == &']' || c == &',';
    let mut packet_iter = input.chars().filter(valid_char).peekable();
    build_tree(&mut packet_iter)
}

fn ordered_pair(pair: &[&str]) -> bool {
    let first_tree = build_tree_from_str(pair[0]).unwrap();
    let second_tree = build_tree_from_str(pair[1]).unwrap();
    first_tree.partial_cmp(&second_tree).unwrap_or(Ordering::Equal).is_le()
}

pub fn part1(input: &str) {
    let input = input.lines().filter(|l| !l.is_empty()).collect::<Vec<&str>>();
    let pairs = input.chunks(2);
    let comp_results = pairs.map(ordered_pair);
    let sum = comp_results.enumerate().filter(|e| e.1).map(|e| e.0 + 1).sum::<usize>();
    println!("{}", sum);
}

pub fn part2(input: &str) {
    let mut input = input.lines().filter(|l| !l.is_empty()).collect::<Vec<&str>>();
    input.push("[[2]]");
    input.push("[[6]]");
    input.sort_by(|a, b| {
        let tree_a = build_tree_from_str(a).unwrap();
        let tree_b = build_tree_from_str(b).unwrap();
        tree_a.partial_cmp(&tree_b).unwrap()
    });
    let product = {
        let mut iter = input.iter().enumerate();
        (iter.find(|x| x.1 == &"[[2]]").unwrap().0 + 1)
        *
        (iter.find(|x| x.1 == &"[[6]]").unwrap().0 + 1)
    };
    println!("{}", product);
}

#[cfg(test)]
mod tests {
    use super::build_tree_from_str;
    use super::Tree::*;
    use super::ordered_pair;

    #[test]
    fn empty_packet() {
        let packet = "[]";
        let tree = build_tree_from_str(packet);
        assert_eq!(tree, Some(crate::d13::Tree::Node(vec!())))
    }

    #[test]
    fn one_value() {
        let packet = "[0]";
        let tree = build_tree_from_str(packet);
        assert_eq!(tree, Some(Node(vec!(Leaf(0)))))
    }

    #[test]
    fn mult_values() {
        let packet = "[0, 10, 54, 1]";
        let tree = build_tree_from_str(packet);
        assert_eq!(tree,
            Some(Node(vec!(
                Leaf(0),
                Leaf(10),
                Leaf(54),
                Leaf(1)
            ))))
    }

    #[test]
    fn with_nested_arrays() {
        let packet = "[0, [10, [54, 23], 1]]";
        let tree = build_tree_from_str(packet);
        assert_eq!(tree,
            Some(Node(vec!(
                Leaf(0),
                Node(vec!(
                    Leaf(10),
                    Node(vec!(Leaf(54), Leaf(23))),
                    Leaf(1)
                ))
            ))))
    }

    #[test]
    fn example_one() {
        let pair = &["[1,1,3,1,1]", "[1,1,5,1,1]"];
        assert!(ordered_pair(pair))
    }

    #[test]
    fn example_two() {
        let pair = &["[[1],[2,3,4]]", "[[1],4]"];
        assert!(ordered_pair(pair))
    }

    #[test]
    fn example_three() {
        let pair = &["[9]", "[[8,7,6]]"];
        assert!(!ordered_pair(pair))
    }

    #[test]
    fn example_four() {
        let pair = &["[[4,4],4,4]", "[[4,4],4,4,4]"];
        assert!(ordered_pair(pair))
    }

    #[test]
    fn example_five() {
        let pair = &["[7, 7, 7, 7]", "[7, 7, 7]"];
        assert!(!ordered_pair(pair))
    }

    #[test]
    fn example_six() {
        let pair = &["[]", "[3]"];
        assert!(ordered_pair(pair))
    }

    #[test]
    fn example_seven() {
        let pair = &["[[[]]]", "[[]]"];
        assert!(!ordered_pair(pair))
    }

    #[test]
    fn example_eight() {
        let pair = &["[1,[2,[3,[4,[5,6,7]]]],8,9]", "[1,[2,[3,[4,[5,6,0]]]],8,9]"];
        assert!(!ordered_pair(pair))
    }

    #[test]
    fn empty_first_message() {
        let pair = &["[[[]]]", "[[]]"];
        assert!(!ordered_pair(pair))
    }

}