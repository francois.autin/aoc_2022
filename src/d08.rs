use std::collections::HashSet;

fn row_vector(row: &str) -> Vec<i32> {
    row.chars().map(|c| {
        c.to_digit(10).unwrap_or_default() as i32
    }).collect()
}

fn input_to_grid(input: &str) -> Vec<Vec<i32>> {

    let input_rows: Vec<&str> = input.lines().collect();
    let mut grid = Vec::<Vec<i32>>::new();

    input_rows.iter().for_each(|row| grid.push(row_vector(row)));

    grid

}

fn scan_grid(grid: &Vec<Vec<i32>>) -> HashSet<(i32, i32)> {

    let mut visible_trees = HashSet::<(i32, i32)>::new();

    // Scanning rows 0 -> row.length()
    let mut y = 0;
    grid.iter().for_each(|row| {
        let mut x = 0;
        let mut tallest_tree_yet = 0;

        row.iter().for_each(|tree| {
            if x == 0 || *tree > tallest_tree_yet { tallest_tree_yet = *tree; visible_trees.insert((x, y)); };
            x += 1;
        });
        y += 1;
    });

    // Scanning rows row.length() -> 0
    let mut y = 0;
    grid.iter().for_each(|row| {
        let mut x = 0;
        let mut tallest_tree_yet = 0;
        let mut reversed_row = row.clone(); reversed_row.reverse();

        reversed_row.iter().for_each(|tree| {
            if x == 0 || *tree > tallest_tree_yet { tallest_tree_yet = *tree; visible_trees.insert((grid[y as usize].len() as i32 - 1-x, y)); };
            x += 1;
        });
        y += 1;
    });

    // Scanning columns 0 -> col.length()
    let mut tallest_tree_yet;
    for x in 0..grid[0].len() {
        tallest_tree_yet = 0;
        for (y, item) in grid.iter().enumerate() {
            if y == 0 || item[x] > tallest_tree_yet { tallest_tree_yet = item[x]; visible_trees.insert((x as i32, y as i32)); };
        }
    };

    // Scanning columns col.length() -> 0
    let mut tallest_tree_yet;
    for x in 0..grid[0].len() {
        tallest_tree_yet = 0;
        for y in 0..grid.len() {
            let coord_y = grid.len()-y-1;
            if y == 0 || grid[coord_y][x] > tallest_tree_yet { tallest_tree_yet = grid[coord_y][x]; visible_trees.insert((x as i32, coord_y as i32)); };
        }
    };

    visible_trees

}

pub fn part1(input: &str) {
    let grid = input_to_grid(input);
    let visible_trees = scan_grid(&grid);
    println!("{}", visible_trees.len() as i32);
}

fn scenic_score(coordinates: (i32, i32), grid: &Vec<Vec<i32>>) -> i32 {

    let size = grid[coordinates.1 as usize][coordinates.0 as usize];
    
    let a = {
        if coordinates.0 != 0 {
            let mut ret = 0;
            for i in 1..(coordinates.0 + 1) {
                if grid[coordinates.1 as usize][(coordinates.0 - i) as usize] < size { ret += 1; }
                else { ret += 1; break; }
            }
            ret
        } else {
            0
        }
    };

    let b = {
        if coordinates.0 != grid[coordinates.1 as usize].len() as i32 - 1 {
            let mut ret = 0;
            for i in (coordinates.0 + 1) as usize..grid[coordinates.1 as usize].len() {
                if grid[coordinates.1 as usize][i] < size { ret += 1; }
                else { ret += 1; break; }
            }
            ret
        } else {
            0
        }
    };

    let c = {
        if coordinates.1 != 0 {
            let mut ret = 0;
            for i in 1..(coordinates.1 + 1) {
                if grid[(coordinates.1 - i) as usize][coordinates.0 as usize] < size { ret += 1; }
                else { ret += 1; break; }
            }
            ret
        } else {
            0
        }
    };

    let d = {
        if coordinates.1 != grid.len() as i32 - 1 {
            let mut ret = 0;
            for item in grid.iter().skip((coordinates.1 + 1) as usize) {
                if item[coordinates.0 as usize] < size { ret += 1; }
                else { ret += 1; break; }
            }
            ret
        } else {
            0
        }
    };

    a*b*c*d

}

fn best_view(coordinates: (i32, i32), grid: &Vec<Vec<i32>>) -> i32 {
    
    if coordinates.0 < 0 || coordinates.1 < 0 || coordinates.1 >= grid.len() as i32 {
        0
    } else if coordinates.0 >= grid[coordinates.1 as usize].len() as i32 {
        best_view((0, coordinates.1 + 1), grid)
    } else {
        let score = scenic_score(coordinates, grid);
        let bv = best_view((coordinates.0 + 1, coordinates.1), grid);
        if score >= bv { score } else { bv }
    }

}

pub fn part2(input: &str) {
    let grid = input_to_grid(input);
    println!("{}", best_view((0, 0), &grid));
}
