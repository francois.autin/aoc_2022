type Method = dyn Fn (&mut Vec<Vec<char>>, String) -> &Vec<Vec<char>>;

fn init_stacks(s: String) -> Vec<Vec<char>> {

    // Initializing return array
    let mut stacks = Vec::<Vec<char>>::new();
    // Tracking position of crates to push to stack
    let mut index = 1;

    // For all 9 stacks
    for _ in 0..9 {

        let mut new_stack = Vec::<char>::new();
        let lines = s.lines();

        // For each line, check the character at the relevant index
        // If it is an alphabetical char, add it to the stack
        lines.for_each(|level| {
            let crate_label = level.char_indices().find(|c| c.0 == index);
            if let Some(c) = crate_label {
                if c.1.is_alphabetic() { new_stack.push(c.1) }
            }
        });

        // Once we've dealt with a stack, we set the index to the next stack
        index += 4;
        // We need to reverse the stack as we've added items upside down
        new_stack.reverse();
        // We then add the stack to the list of stacks
        stacks.push(new_stack)

    };

    stacks
    
}

fn line_to_instruction(line: &str) -> (i32, i32, i32) {
    let mut i = line.split_terminator(|x: char| !x.is_numeric())
                    .filter(|x| !x.is_empty())
                    .map(|x| x.parse::<i32>().unwrap());
    (i.next().unwrap(), i.next().unwrap(), i.next().unwrap())
}

fn reorder_stacks_part1(stacks: &mut Vec<Vec<char>>, s: String) -> &Vec<Vec<char>> {

    let instructions = s.lines().map(line_to_instruction);

    // Pour chaque instruction, on remplit la stack transportée par la grue avec:
    // - le nombre donné de crates à ramasser (insutrction.0)
    // - le l'index de la stack source (instruction.1)
    // Puis on dépose la charge sur la stack cible (index instruction.2)
    instructions.for_each(|instruction| {
        // On charge la grue
        let crane_payload = {
            let mut load = Vec::<char>::new();
            for _ in 0..instruction.0 {
                load.push(stacks[(instruction.1-1) as usize].pop().unwrap());
            };
            load
        };
        // On la décharge sur la stack cible
        crane_payload.iter()
            .for_each(|x| stacks[(instruction.2-1) as usize].push(*x))
    });

    stacks

}

fn reorder_stacks_part2(stacks: &mut Vec<Vec<char>>, s: String) -> &Vec<Vec<char>> {

    let instructions = s.lines().map(line_to_instruction);

    // Pour chaque instruction, on remplit la stack transportée par la grue avec:
    // - le nombre donné de crates à ramasser (insutrction.0)
    // - le l'index de la stack source (instruction.1)
    // Puis on dépose la charge sur la stack cible (index instruction.2)
    instructions.for_each(|instruction| {
        // On charge la grue
        let crane_payload = {
            let mut load = Vec::<char>::new();
            for _ in 0..instruction.0 {
                load.push(stacks[(instruction.1-1) as usize].pop().unwrap());
            };
            // On inverse pour respecter l'ordre initial des crates
            load.reverse();
            load
        };
        // On la décharge sur la stack cible
        crane_payload.iter()
            .for_each(|x| stacks[(instruction.2-1) as usize].push(*x))
    });

    stacks

}

fn reorder(input: &str, reorder_stacks: &Method) -> String {

    // Adding all non-empty lines without the move keyword to the initial_stack_arrangement_input string
    let initial_stack_arrangement_input = {
        let mut s = String::new();
        input.lines().filter(|x| !x.contains("move") && !x.is_empty()).for_each(|x| { s.push_str(x); s.push('\n'); });
        s
    };

    // Building the initial stacks
    let mut stacks: Vec<Vec<char>> = init_stacks(initial_stack_arrangement_input);

    // Adding all lines containing the move keyword to the reorder_input string
    let reorder_input = {
        let mut s = String::new();
        input.lines().filter(|x| x.contains("move")).for_each(|x| { s.push_str(x); s.push('\n'); });
        s
    };

    // Building the reordered stacks from the initial stacks and the reordering instructions
    let reordered_stacks = reorder_stacks(&mut stacks, reorder_input);

    // Building output string
    // Appending the last char element of each stack to the string
    let mut output = String::new();
    reordered_stacks.iter().for_each(|stack| {
        if let Some(x) = stack.last() {
            output.push(*x)
        }
    });

    output

}

pub fn part1(input: &str)
{
    println!("{}", reorder(input, &reorder_stacks_part1))
}

pub fn part2(input: &str)
{
    println!("{}", reorder(input, &reorder_stacks_part2))
}