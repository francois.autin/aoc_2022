fn part1_round_score(round: &str) -> i32 {

    let round_result = match round {
        // Draw
        "A X" | "B Y" | "C Z" => 3,
        // Lose
        "A Z" | "B X" | "C Y" => 0,
        // Win
        "A Y" | "B Z" | "C X" => 6,
        // Default case
        _ => 0
    };

    let picked_move_score = match round.chars().last().unwrap() {
        'X' => 1,
        'Y' => 2,
        'Z' => 3,
        _ => 0
    };

    round_result + picked_move_score

}


fn part2_figuring_out_moves_to_pick(round: &str) -> i32 {

    let opponent_pick = round.chars().next().unwrap();

    match round.chars().last().unwrap() {
        // Draw
        'Y' => 3 + match opponent_pick { 'A' => 1, 'B' => 2, 'C' => 3, _ => 0 },
        // Lose
        'X' => match opponent_pick { 'A' => 3, 'B' => 1, 'C' => 2, _ => 0},
        // Win
        'Z' => 6 + match opponent_pick { 'A' => 2, 'B' => 3, 'C' => 1, _ => 0},
        // Default case
        _ => 0
    }

}

pub fn part1(input: &str)
{
    println!("{}", input.lines().fold(0, |acc, x| acc + part1_round_score(x)))
}

pub fn part2(input: &str)
{
    println!("{}", input.lines().fold(0, |acc, x| acc + part2_figuring_out_moves_to_pick(x)))
}
