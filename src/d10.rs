fn exec(instrution: &str, mem: &i32) -> i32
{
    mem + if let Some(("addx", r)) = instrution.split_once(' ') { r.parse::<i32>().unwrap_or_default() } else { 0 }
}

fn get_signal_strength(memory_history: &[i32], at: i32) -> i32
{
    at * memory_history[(at - 1) as usize]
}

fn get_signal_strength_sum(memory_history: Vec<i32>) -> i32
{
    let markers = [20, 60, 100, 140, 180, 220];
    markers.iter().map(|x| get_signal_strength(&memory_history, *x)).sum()
}

fn get_memory_history(input: &str) -> Vec<i32>
{
    let mut memory_history = [1].to_vec();
    input.lines()
        .flat_map(|x| if x == "noop" { ["noop"].to_vec() } else { ["noop", x].to_vec() })
        .for_each(|i| memory_history.push(exec(i, memory_history.last().unwrap())));
    memory_history
}

fn draw_line(memory_history: &[i32], x: i32, y: i32)
{
    let sprite_position = memory_history.get((y * 40 + x) as usize).unwrap();
    if (sprite_position - x).abs() <= 1 { print!("#") } else { print!(".") }
}

fn display_image(memory_history: Vec<i32>)
{
    for line in 0..6 {
        for col in 0..41 {
            draw_line(&memory_history, col, line);
        }
        println!();
    };
}

pub fn part1(input: &str)
{
    let memory_history = get_memory_history(input);
    println!("{}", get_signal_strength_sum(memory_history));
}

pub fn part2(input: &str)
{
    let memory_history = get_memory_history(input);
    println!();
    display_image(memory_history);
}