use std::{fs::File, path::Path, io::Read, env};
mod d01; mod d02; mod d03; mod d04; mod d05;
mod d06; mod d07; mod d08; mod d09; mod d10;
mod d11; mod d12; mod d13; mod d14;

type Parts<'a> = (&'a dyn Fn(&str), &'a dyn Fn(&str));

fn read_input(path: String) -> String
{
    let mut buf = String::new();
    let mut f = File::open(Path::new(&(path))).unwrap();
    f.read_to_string(&mut buf).unwrap();
    buf
}

fn get_fun(day: &u8) -> Parts
{
    match day {
        1  => (&d01::part1, &d01::part2),
        2  => (&d02::part1, &d02::part2),
        3  => (&d03::part1, &d03::part2),
        4  => (&d04::part1, &d04::part2),
        5  => (&d05::part1, &d05::part2),
        6  => (&d06::part1, &d06::part2),
        7  => (&d07::part1, &d07::part2),
        8  => (&d08::part1, &d08::part2),
        9  => (&d09::part1, &d09::part2),
        10 => (&d10::part1, &d10::part2),
        11 => (&d11::part1, &d11::part2),
        12 => (&d12::part1, &d12::part2),
        13 => (&d13::part1, &d13::part2),
        14 => (&d14::part1, &d14::part2),
        _ => panic!("Day {day} not yet implemented!")
    }
}

fn which_days_to_run() -> Vec<u8>
{
    // If none is supplied, all days will run
    if env::args().len() == 1 {
        let mut a = [0; 14]; for (elem, val) in a.iter_mut().zip(1..=25) { *elem = val }; a.to_vec()
    }
    // Otherwise, we parse through the integers supplied as args
    else {
        env::args().filter_map(|x| {
            match x.trim().parse::<u8>() {
                Ok(n) => Some(n),
                Err(_) => None
            }
        })
        .filter(|x| *x > 0 && *x < 26)
        .collect()
    }
}

fn run(day: &u8)
{
    let d_fmt = format!("{:02}", day);
    let s = read_input("./input/d".to_string() + &d_fmt);
    let f = get_fun(day);
    println!("====== Day {d_fmt} ======");
    print!("Part 1: "); f.0(&s);
    print!("Part 2: "); f.1(&s);
    println!("====================\n");
}

fn main()
{
    let days = which_days_to_run();
    days.iter().for_each(run)
}
