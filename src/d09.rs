use std::collections::HashSet;

/// Generates a path from the input such as:
/// <dir> <len> => <dir><dir>...*len
/// U 5 => UUUUU
fn path_from_input(input: &str) -> String
{
    fn line_to_subpath(line: &str) -> String
    {
        let (dir, len) = {
            let split = line.split_at(1);
            (split.0, split.1.trim().parse::<i32>().unwrap_or_default())
        };
        let mut s = String::new();
        for _ in 0..len { s.push_str(dir) };
        s
    }
    let mut path = String::new();
    input.lines().map(line_to_subpath).for_each(|x| path.push_str(x.as_str()));
    path
}

/// Creates a new head moved towards a given direction
/// update_head((O, O), 'U') -> (O, 1)
fn update_head(knot: (i32, i32), dir: char) -> (i32, i32)
{
    let mut new_knot = knot;
    match dir {
        'R' => new_knot.0 += 1,
        'D' => new_knot.1 -= 1,
        'L' => new_knot.0 -= 1,
        'U' => new_knot.1 += 1,
        e => panic!("Illegal character: {}", e)
    };
    new_knot
}

/// Checks if a tail is eligible for being updated
/// is_pulled(&(0, 0), &(1, 0)) -> false
/// is_pulled(&(0, 0), &(2, 0)) -> true
fn is_pulled(tail: &(i32, i32), head: &(i32, i32)) -> bool
{
    (head.0 - tail.0).abs() > 1 || (head.1 - tail.1).abs() > 1
}

/// Creates a new tail given the postion of the head
fn move_tail(tail: (i32, i32), head: &(i32, i32)) -> (i32, i32)
{
    if !is_pulled(&tail, head) { tail }
    else {
        let mut ntail = tail;
        if head.0 == tail.0 {
            if head.1 - tail.1 > 1 { ntail.1 += 1 } else { ntail.1 -= 1 }
        } else if head.1 == tail.1 {
            if head.0 - tail.0 > 1 { ntail.0 += 1 } else { ntail.0 -= 1 }
        } else {
            if head.1 - tail.1 >= 1 { ntail.1 += 1 } else { ntail.1 -= 1 }
            if head.0 - tail.0 >= 1 { ntail.0 += 1 } else { ntail.0 -= 1 }
        }
        ntail
    }
}

/// Creates a rope of given length
/// make_rope(5) -> [(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)]
fn make_rope(length: i32) -> Vec<(i32, i32)>
{
    let mut vec = Vec::<(i32, i32)>::new();
    for _ in 0..length { vec.push((0, 0)) };
    vec
}

/// Creates a new updated rope from a rope and a direction for the head
/// First, the head is updated. Then the position of all knots is updated
/// in order.
fn update_rope(rope: &Vec<(i32, i32)>, dir: char) -> Vec<(i32, i32)>
{
    let mut new_rope = Vec::<(i32, i32)>::new();
    new_rope.push(update_head(rope[0], dir));
    for i in 1..rope.len() {
        let tail = rope[i];
        let n_tail = move_tail(tail, &(new_rope)[i - 1].to_owned());
        new_rope.push(n_tail);
    }
    new_rope
}

/// Counts the number of unique positions visited by the tail of a rope
/// of length `length` given a `path` followed by the rope head.
fn positions_visited_by_rope_tail(path: &str, length: i32) -> i32
{
    let mut rope = make_rope(length);
    let mut positions_visited_by_tail = HashSet::new();
    path.chars().for_each(|dir| {
        rope = update_rope(&rope, dir);
        positions_visited_by_tail.insert(*rope.last().unwrap());
    });
    positions_visited_by_tail.len() as i32
}

pub fn part1(input: &str)
{
    let path = path_from_input(input);
    let result = positions_visited_by_rope_tail(&path, 2); 
    println!("{}", result);
}

pub fn part2(input: &str)
{
    let path = path_from_input(input);
    let result = positions_visited_by_rope_tail(&path, 10);
    println!("{}", result);
}