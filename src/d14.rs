use std::iter::Peekable;

const DIM: usize = 1000;

#[derive(Debug, Clone, PartialEq)]
enum Space {
    Empty, Occupied
}

enum NextPosition {
    B, BL, BR, Same
}

type Scan = Vec<Vec<Space>>;
type Coord = (usize, usize);

trait Formation {
    fn parse_coord<'a>(s: &'a str) -> Coord;
    fn add_rock(&mut self, input: Coord);
    fn move_towards<'a>(ibr: Coord, next_rock: &Coord) -> Coord;
    fn build_path<'a, T>(&mut self, input: Peekable<T>) where T: Iterator<Item = &'a str>;
    fn add_formation(&mut self, input: &str);
}

impl Formation for Scan {
    fn parse_coord<'a>(s: &'a str) -> Coord {
        let mut parsed_coord = s.split(',').map(str::parse::<usize>).map(Result::unwrap);
        (parsed_coord.next().unwrap(), parsed_coord.next().unwrap())
    }

    fn add_rock(&mut self, coord: Coord) {
        self[coord.0][coord.1] = Space::Occupied;
    }

    fn move_towards<'a>(mut ibr: Coord, nr: &Coord) -> Coord {
        match (ibr.0 == nr.0, ibr.1 == nr.1) {
            (false, true) => ibr.0 = if ibr.0 < nr.0 { ibr.0 + 1 } else { ibr.0 - 1 },
            (true, false) => ibr.1 = if ibr.1 < nr.1 { ibr.1 + 1 } else { ibr.1 - 1 },
            _ => panic!("Not supposed to happen")
        };
        ibr
    }

    fn build_path<'a, T>(&mut self, mut input: Peekable<T>) where T: Iterator<Item = &'a str> {
        while let Some(rock) = input.next() {
            let mut coord = Self::parse_coord(rock);
            self.add_rock(coord);
            match input.peek() {
                Some(next_rock) => {
                    let nr = Self::parse_coord(&next_rock);
                    while coord != nr {
                        self.add_rock(coord);
                        coord = Self::move_towards(coord, &nr);
                    }
                },
                None => ()
            }
        };
    }

    fn add_formation(&mut self, input: &str) {
        let path = input.split(" -> ").peekable();
        self.build_path(path)
    }
}

trait SandSimulation {
    fn where_move(&mut self, unit: Coord) -> Option<NextPosition>;
    fn simulate(&mut self) -> i32;
}

impl SandSimulation for Scan {
    fn where_move(&mut self, unit: Coord) -> Option<NextPosition> {
        use Space::*;
        use NextPosition::*;
        if unit.1 + 2 > self[0].len() || unit.0 + 1 > self.len(){
            None
        } else {
            Some(
                if self[unit.0][unit.1 + 1] == Empty { B }
                else if self[unit.0 - 1][unit.1 + 1] == Empty { BL }
                else if self[unit.0 + 1][unit.1 + 1] == Empty { BR }
                else { Same }
            )
        }
    }

    fn simulate(&mut self) -> i32 {
        use NextPosition::*;
        let mut nb_resting_sand = 0;
        let origin = (500, 0);
        let mut sand_unit = origin.clone();
        loop {
            match self.where_move(sand_unit) {
                None => break,
                Some(B) => sand_unit.1 = sand_unit.1 + 1,
                Some(BL) => sand_unit = (sand_unit.0 - 1, sand_unit.1 + 1),
                Some(BR) => sand_unit = (sand_unit.0 + 1, sand_unit.1 + 1),
                Some(Same) => {
                    self.add_rock(sand_unit);
                    nb_resting_sand = nb_resting_sand + 1;
                    if sand_unit == origin.clone() {
                        break;
                    } else {
                        sand_unit = origin.clone()
                    }
                },
            }
        };
        nb_resting_sand
    }
}

fn build_scan(input: &str) -> Scan {
    let mut initial_scan = vec![vec![Space::Empty; DIM]; DIM];
    input.lines().for_each(|i| initial_scan.add_formation(i));
    initial_scan
}

pub fn part1(input: &str) {
    let mut scan = build_scan(input);
    let result = scan.simulate();
    println!("{}", result)
}

pub fn part2(input: &str) {
    let floor_id = input.lines().flat_map(|l| l.split(" -> "))
                 .map(|x| Scan::parse_coord(x).1).max().unwrap() + 2;
    let mut scan = build_scan(input);
    scan.add_formation(&format!("0,{} -> {},{}", floor_id, DIM-1, floor_id));
    let result = scan.simulate();
    println!("{}", result)
}