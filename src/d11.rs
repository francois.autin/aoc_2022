/// Monkey state
struct Monkey
{   
    /// The items currently held by the monkey
    items: Vec::<i64>,
    /// The operation the monkey does to the currently inspected item
    op: Box<dyn Fn (i64) -> i64>,
    /// The number to check against the worry level
    divisible_by: i64,
    /// .0: the monkey to give the item to, .1: the item to give
    where_to: (u8, u8),
    /// The number of inspections
    inspections: i64
}

/// Monkey operations
impl Monkey
{
    fn _print(&self)
    {
        println!(" ________________________________________________________________________ ");
        println!("| Monkey:                                                                 ");
        println!("| Stating items: {:?}                                                     ", self.items);
        println!("| Test: divisible by {}                                                   ", self.divisible_by);
        println!("|     If true: {}                                                         ", self.where_to.0);
        println!("|     If false: {}                                                        ", self.where_to.1);
        println!(" ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯ ");
    }
    /// Part 1 inspection: the worry level is divided by three
    fn inspect(&mut self, item: i64) -> (u8, i64)
    {
        self.inspections += 1;
        let _ = self.items.remove(0);
        let worry_level = (self.op)(item) / 3;
        if worry_level % self.divisible_by == 0 {
            (self.where_to.0, worry_level)
        } else {
            (self.where_to.1, worry_level)
        }
    }
    /// Part 2 inspection: the worry level is given wholly modulo the product of all Monkey.divisible_by (limit)
    fn inspect_dangerously(&mut self, item: i64, limit: i64) -> (u8, i64)
    {
        self.inspections += 1;
        let _ = self.items.remove(0);
        let worry_level = (self.op)(item);
        if worry_level % self.divisible_by == 0 {
            (self.where_to.0, worry_level % limit)
        } else {
            (self.where_to.1, worry_level % limit)
        }
    }
    /// Gives an item to this monkey
    fn give(&mut self, item: i64)
    {
        self.items.push(item)
    }
}

/// Parses the items held by a monkey from the input
fn get_items(description: &str) -> Option<Vec<i64>>
{
    Some(description.lines()
        .find(|x| x.contains("Starting items:"))?
        .trim()
        .trim_start_matches(|c: char| !c.is_numeric())
        .split_terminator(", ")
        .map(|s| s.parse::<i64>().unwrap_or_default())
        .collect())
}

/// Parses the operation a monkey does from the input
fn get_op(description: &str) -> Option<Box<dyn Fn(i64) -> i64>>
{
    let (op, add) = {
        description.lines()
            .find(|x| x.contains("Operation:"))?
            .trim()
            .trim_start_matches(|c: char| !['+',  '*'].contains(&c))
            .split_at(1)
    };
    match (op, add.trim()) {
        ("*", "old") => Some(Box::new(|n| n * n)),
        ("*", s) => { let a = s.parse::<i64>().unwrap_or_default(); Some(Box::new(move |n| n * a)) },
        ("+", "old") => Some(Box::new(|n| n + n)),
        ("+", s) => { let a = s.parse::<i64>().unwrap_or_default(); Some(Box::new(move |n| n + a)) }
        _ => None
    }
}

/// Parses the number to check against the item's worry level from the input
fn get_div(description: &str) -> Option<i64>
{
    Some(description.lines()
        .find(|x| x.contains("Test:"))?
        .trim()
        .trim_start_matches(|c: char| !c.is_numeric())
        .parse::<i64>().unwrap_or_default()
    )
}

/// Parses the number of the monkeys to throw the item to depending on the check result
fn get_where_to(description: &str) -> Option<(u8, u8)>
{
    let mut branch = description.lines()
        .filter(|x| x.contains("If "))
        .map(|line| line.trim().trim_start_matches(|c: char| !c.is_numeric()))
        .map(|s| s.parse::<u8>().unwrap_or_default());
    Some((branch.next()?, branch.next()?))
}

/// Input parsing method. Instanciates a Monkey from a slice of the input
fn desc_to_monkey(description: &str) -> Option<Monkey>
{
    Some(Monkey {
        items: get_items(description)?,
        op: get_op(description)?,
        divisible_by: get_div(description)?,
        where_to: get_where_to(description)?,
        inspections: 0
    })
}

/// Input parsing method. Splits the input into relevant slices and
/// returns a Vector containing all monkeys and their initial states.
fn init_monkeys(input: &str) -> Vec<Monkey>
{
    input.split_terminator("\n\n")
        .map(desc_to_monkey)
        .map(|m| match m { Some(a) => a, None => panic!("Improper parsing!") })
        .collect()
}

fn round_part1(monkeys: &mut Vec<Monkey>)
{
    for i in 0..monkeys.len() {
        let items = monkeys[i].items.to_owned();
        items.iter().for_each(|item| {
            let (to, what) = monkeys[i].inspect(*item);
            monkeys[to as usize].give(what)
        });
    };
}

fn round_part2(monkeys: &mut Vec<Monkey>)
{
    let limit = monkeys.iter().map(|m| m.divisible_by).product();
    for i in 0..monkeys.len() {
        let items = monkeys[i].items.to_owned();
        items.iter().for_each(|item| {
            let (to, what) = monkeys[i].inspect_dangerously(*item, limit);
            monkeys[to as usize].give(what);
        })
    }
}

fn get_monkey_business(monkeys: Vec<Monkey>) -> i64
{
    let mut monkey_business: Vec<i64> = monkeys.iter().map(|m| m.inspections).collect();
    monkey_business.sort();
    monkey_business.pop().unwrap_or_default() * monkey_business.pop().unwrap_or_default()
}

pub fn part1(input: &str)
{
    let mut monkeys = init_monkeys(input);
    for _ in 0..20 { round_part1(&mut monkeys) }
    let result = get_monkey_business(monkeys);
    println!("{result}")
}

pub fn part2(input: &str)
{
    let mut monkeys = init_monkeys(input);
    for _ in 0..10000 { round_part2(&mut monkeys) }
    let result = get_monkey_business(monkeys);
    println!("{result}")
}